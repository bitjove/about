<?php

namespace AboutYou;

/**
 * Bacis test for the FactoryProductServiceSeeder
 *
 * @author Jove Trajkoski
 *
 */
use AboutYou\ProductServices\Services\FactoryProductServiceSeeder;
use AboutYou\ProductServices\Services\PriceOrderedProductService;
use AboutYou\ProductServices\Services\UnorderedProductService;

class FactoryProductServiceSeederTest  extends PHPUnit_Framework_TestCase{

    public function testCreateUnorderedProductService(){
        $data = new FactoryProductServiceSeeder();
        $data = $data->for('UnorderedProducts');
        $this->assertTrue($data instanceof UnorderedProductService);
    }

    public function testCreatePriceOrderedProductService(){
        $data = new FactoryProductServiceSeeder();
        $data = $data->for('PriceOrderedProducts');
        $this->assertTrue($data instanceof PriceOrderedProductService);
    }
} 