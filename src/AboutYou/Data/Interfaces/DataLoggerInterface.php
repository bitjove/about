<?php

namespace AboutYou\Data\Interfaces;

interface DataLoggerInterface
{
    /**
     * This method should read from a data source like Database logger,
     * File logger etc. (JSON in our case)
     *
     * @return data
     */
    public function get();
}
