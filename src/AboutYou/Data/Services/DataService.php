<?php

namespace AboutYou\Data\Services;

use AboutYou\Data\Interfaces\DataLoggerInterface;

/**
 * This class is Concrete class that will never be changed,  
 * we can just pass different classes in the constructor,
 * depending on the data we want to work with.
 */
class DataService
{

	/**
     * @var DataLoggerInterface
     */
    protected $dataLogger;

    /**
     * @param DataLoggerInterface $dataLogger
     */
    public function __construct(DataLoggerInterface $dataLogger)
    {
        $this->dataLogger = $dataLogger;
    }

    /**
     * @inheritdoc
     */
    public function get()
    {
        return $this->dataLogger->get();
    }
}
