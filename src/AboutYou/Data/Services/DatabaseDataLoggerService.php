<?php

namespace AboutYou\Data\Services;

use AboutYou\Data\Interfaces\DataLoggerInterface;

/**
 * This class is a Database specific example implementation.
 */
class DatabaseDataLoggerService implements DataLoggerInterface
{

    /**
     * @inheritdoc
     */
    public function get()
    {
        // Query object to read from database in this example.
        // ProductsQuery::findOrFail(17325)->getProducts();
    }
}
