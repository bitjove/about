<?php

namespace AboutYou\Data\Services;

use AboutYou\Data\Interfaces\DataLoggerInterface;

/**
 * This class is a specific example of Json Data implementation.
 */
class JsonDataLoggerService implements DataLoggerInterface
{
	
	/**
     * Path to the json file that contains the Data to work with
     * @var string
     */
    public $dataPath = 'data/17325.json';

    /**
     * Data Source
     * @var array data
     */
    protected $data;

    /**
     * Manipulated DATA
     * @var mixed
     */
    protected $result;

    /**
     * Load Data in json format.
     */
    public function __construct()
    {
        $this->data[] = $this->get();
        // echo '<pre>' . print_r($this->data, true) . '</pre>';
    }

    /**
     * @inheritdoc
    */
    public function get()
    {
        $json_content = file_get_contents($this->dataPath);
        $json = json_decode($json_content, true);

        return $json;
    }

    /**
     * Set the data path file
     * @param $dataPath
     */
    public function setDataPath($dataPath)
    {
        $this->dataPath = $dataPath;
    }

    /**
     * Loading data if the file exist
     * @return bool
     */
    public function loadData()
    {
    	if (file_exists($this->dataPath)) {
            $json_content = file_get_contents($this->dataPath);
            $this->data[] = json_decode($json_content, true);
            return true;
        }
        return false;
    }

    /**
     * return manipulated Data
     * @return mixed
     */
    public function result()
    {
        return $this->result;
    }

    /**
     * Casts result in json format
     * @return string json
     */
    public function resultToJson()
    {
        return json_encode($this->result());
    }
}
