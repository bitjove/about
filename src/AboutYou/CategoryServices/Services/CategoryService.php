<?php

namespace AboutYou\CategoryServices\Services;

use AboutYou\App;
use AboutYou\CategoryServices\Interfaces\CategoryServiceInterface;
use AboutYou\Entity\Product;
use AboutYou\Entity\Variant;
use AboutYou\Entity\Price;

use AboutYou\EntitiesValidators\VariantValidator;

/**
 * Basic implementation of the CategoryService with CategoryServiceInterface interface.
 */
class CategoryService implements CategoryServiceInterface
{
    /**
     * Data Source
     * @var array data
     */
    protected $data = [];

    /**
     * Product Source
     * @var array products
     */
    private $products = [];

    /**
     * Variant Source
     * @var array variants
     */
    private $variants = [];

    /**
     * Price Source
     * @var array prices
     */
    private $prices = [];

    /**
     * Json data is binded as a "provider" in index.php file, so we need to resolve that
     * out of the IOC container
     */
    public function __construct()
    {
        $this->data = App::resolve('jsonData'); // json_decode(file_get_contents('data/17325.json')); vaka?
    }

    /**
     * @inheritdoc
     */
    public function getProducts($categoryId)
    {
        // Check if data wasn't provided and return empty array of products
        if (empty($this->data)) {
            return $this->products;
        }

        // Check if categoryDataId correspond to the requested categoryId
        if ($this->data['id'] !== $categoryId) {
            return $this->products;
        }

        foreach ($this->data['products'] as $keyProduct => $product) {
            $productEntity = new Product($keyProduct, $product['name'], $product['description']);
            $this->iterateOverVariants($product['variants'], $productEntity);
            $this->products[] = $productEntity;
        }
        return $this->products;
    }

    /**
     * Iterate and pass the corresponding classes from the Entity namespace.
     */
    private function iterateOverVariants($variantsArray, $productEntity)
    {   
        foreach ($variantsArray as $keyVariant => $variant) {
            $variantEntity = new Variant($keyVariant, $variant['isDefault'], $variant['isAvailable'], $variant['quantity'], $variant['size']);
            VariantValidator::validate($variantEntity);
            $productEntity->setVariants($variantEntity);
            $priceEntity = new Price($variant['price']['current'], $variant['price']['old'], $variant['price']['isSale']);
            $variantEntity->setPrice($priceEntity);
        }
    }
}
