<?php

namespace AboutYou\Entity;

// We can extend from some class to use php magic methods but I don't want to complicate that much.
class Category
{
    /**
     * Id of the Category.
     *
     * @var int
     */
    private $id;

    /**
     * Name of the Category.
     *
     * @var string
     */
    private $name;

    /**
     * List of Products that belong to a Category.
     *
     * @var \AboutYou\Entity\Product[]
     */
    private $products = [];

    /**
     * Initiates category object from given id and name values
     *
     * @param  integer $id
     * @param  string $name
     */
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return \AboutYou\Entity\Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param \AboutYou\Entity\Product[] $products
     *
     * @return self
     */
    public function setProducts(\AboutYou\Entity\Product $products)
    {
        $this->products = $products;

        return $this;
    }
}
