<?php

namespace AboutYou\Entity;


// We can extend from some class to use php magic methods but I don't want to complicate that much.
class Product
{
    /**
     * Id of the Product.
     *
     * @var int
     */
    private $id;

    /**
     * Name of the Product.
     *
     * @var string
     */
    private $name;

    /**
     * Description of the Product.
     * 
     * @var string
     */
    private $description;

    /**
     * Unsorted list of Variants with their corresponding prices.
     * 
     * @var \AboutYou\Entity\Variant[]
     */
    private $variants = [];

    /**
     * Initiates product object from given id, name and description values
     *
     * @param  integer $id
     * @param  string $name
     * @param  string $description
     */
    public function __construct($id, $name, $description)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return \AboutYou\Entity\Variant[]
     */
    public function getVariants()
    {
        return $this->variants;
    }

    /**
     * @param \AboutYou\Entity\Variant[] $variants
     *
     * @return self
     */
    public function setVariants($variants)
    {
        array_push($this->variants, $variants);

        return $this;
    }
}
