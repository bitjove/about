<?php

namespace AboutYou\Entity;

// We can extend from some class to use php magic methods but I don't want to complicate that much.
class Price
{
    /**
     * Current price.
     *
     * @var int
     */
    private $current;

    /**
     * Old price.
     *
     * @var int|null
     */
    private $old;

    /**
     * Defines if the price is sale.
     *
     * @var bool
     */
    private $isSale;

    /**
     * Variant that the price belongs to.
     *
     * @var \AboutYou\Entity\Variant
     */
    private $variant;

    /**
     * Initiates price object from given current, old and isSale values
     *
     * @param  integer $current
     * @param  int|null $old
     * @param  bool $isSale
     */
    public function __construct($current, $old, $isSale)
    {
        $this->current = $current;
        $this->old = $old;
        $this->isSale = $isSale;
    }

    /**
     * @return int
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * @param int $current
     *
     * @return self
     */
    public function setCurrent($current)
    {
        $this->current = $current;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getOld()
    {
        return $this->old;
    }

    /**
     * @param int|null $old
     *
     * @return self
     */
    public function setOld($old)
    {
        $this->old = $old;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsSale()
    {
        return $this->isSale;
    }

    /**
     * @param bool $isSale
     *
     * @return self
     */
    public function setIsSale($isSale)
    {
        $this->isSale = $isSale;

        return $this;
    }

    /**
     * @return \AboutYou\Entity\Variant
     */
    public function getVariant()
    {
        return $this->variant;
    }

    /**
     * @param \AboutYou\Entity\Variant $variant
     *
     * @return self
     */
    public function setVariant(\AboutYou\Entity\Variant $variant)
    {
        $this->variant = $variant;

        return $this;
    }
}
