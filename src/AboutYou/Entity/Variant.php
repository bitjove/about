<?php

namespace AboutYou\Entity;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints as Assert;

// We can extend from some class to use php magic methods but I don't want to complicate that much.
class Variant
{
    /**
     * Id of the Variant.
     *
     * @var int
     */
    private $id;

    /**
     * Defines if the Variant is default for the product.
     *
     * @var bool
     */
    private $isDefault;

    /**
     * Defines if the Variant is Available or not.
     * 
     * @var bool
     */
    private $isAvailable;

    /**
     * Number of available items in stock.
     *
     * @var int
     */
    private $quantity;

    /**
     * Size of the Variant.
     *
     * @var mixed
     */
    private $size;

    /**
     * Variant price.
     * 
     * @var \AboutYou\Entity\Price
     */
    private $price;

    /**
     * Product that the Variant belongs to.
     *
     * @var \AboutYou\Entity\Product
     */
    private $product;


    /**
     * Initiates variant object from given id, isDefault, isAvailable, quantity and size values
     *
     * @param  integer $id
     * @param  bool $isDefault
     * @param  string $isAvailable
     * @param  integer $quantity
     * @param  mixed $size
     */
    public function __construct($id, $isDefault, $isAvailable, $quantity, $size)
    {
        $this->id = $id;
        $this->isDefault = $isDefault;
        $this->isAvailable = $isAvailable;
        $this->quantity = $quantity;
        $this->size = $size;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * @param bool $isDefault
     *
     * @return self
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsAvailable()
    {
        return $this->isAvailable;
    }

    /**
     * @param bool $isAvailable
     *
     * @return self
     */
    public function setIsAvailable($isAvailable)
    {
        $this->isAvailable = $isAvailable;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     *
     * @return self
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     *
     * @return self
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return \AboutYou\Entity\Price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param \AboutYou\Entity\Price $price
     *
     * @return self
     */
    public function setPrice(\AboutYou\Entity\Price $price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return \AboutYou\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param \AboutYou\Entity\Product $product
     *
     * @return self
     */
    public function setProduct(\AboutYou\Entity\Product $product)
    {
        $this->product = $product;

        return $this;
    }
}
