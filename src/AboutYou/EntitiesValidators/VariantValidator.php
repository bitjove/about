<?php

namespace AboutYou\EntitiesValidators;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * This is a basic Symfony validator https://symfony.com/doc/current/components/validator.html
 */
class VariantValidator
{
    /**
     * We can validate fields with this Symfony class
     * For example the length of variantID must be at least 3 characters long (101, 102, 201, 202)
     * it must not be Blank and must be from integer type,
     * We can change the 'min' => 4 for example and violation message will be printed "Woops!! Your Varian Id must be at least thre characters long." 
     */
    public static function validate($variant)
    {
        $validator = Validation::createValidator();

        $errors = $validator->validate($variant);

        $violations = $validator->validate($variant->getId(), array(
            new Length(array('min' => 3,'minMessage' => 'Woops!! Your Varian Id must be at least thre characters long.')),
            new NotBlank(array('message' => 'Sorry boss, Varian Id should not be blank :)')),
            new Assert\Type(array(
                'type'    => 'integer',
                'message' => 'The value {{ value }} is not a valid {{ type }}.'
            ))
        ));

        if (0 !== count($violations)) {
            // there are errors, now you can show them
            foreach ($violations as $violation) {
                echo '<pre>' . print_r($violation->getMessage() , true) . '</pre>';
            }
        }
    }
}
