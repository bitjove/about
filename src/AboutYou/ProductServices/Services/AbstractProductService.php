<?php

namespace AboutYou\ProductServices\Services;

abstract class AbstractProductService
{

	/**
     * @var CategoryServiceInterface
     */
    protected $categoryService;

    /**
     * Maps from category name to the id for the category service.
     *  
     * @var array
     */
    protected $categoryNameToIdMapping = [
        'Clothes' => 17325
    ];

    /**
     * @param CategoryServiceInterface $categoryService
     */
    public function __construct(CategoryServiceInterface $categoryService)
    {
       $this->categoryService = $categoryService;
    }


    /**
     * Prevent from invalid mapping.
     *
     * @param string $categoryName
     *
     * @throws \InvalidArgumentException if category name is unknown.
     */
    abstract public function guardForInvalidMaping($categoryName);

    /**
     * Get Products by Category name.
     *
     * @param string $categoryName
     *
     * @return \AboutYou\Entity\Product[]
     *
     * @throws \InvalidArgumentException if category name is unknown.
     */
    abstract public function getProductsForCategory($categoryName);
}