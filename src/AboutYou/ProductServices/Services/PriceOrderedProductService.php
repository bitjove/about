<?php

namespace AboutYou\ProductServices\Services;

use AboutYou\CategoryServices\Interfaces\CategoryServiceInterface;
use AboutYou\ProductServices\Interfaces\ProductServiceInterface;
// use AboutYou\ProductServices\Services\AbstractProductService;

/**
 * This class is example implementation of price ordered product service.
 * I made an abstract class called AbstractProductService that ProductService classes like this
 * should inherit from, but that can be solved in the near future 
 * when the code is getting jiggy and messy.
 */
class PriceOrderedProductService implements ProductServiceInterface
{
    /**
     * @var CategoryServiceInterface
     */
    private $categoryService;

    /**
     * Maps from category name to the id for the category service.
     *  
     * @var array
     */
    private $categoryNameToIdMapping = [
        'Clothes' => 17325
    ];

    /**
     * @param CategoryServiceInterface $categoryService
     */
    public function __construct(CategoryServiceInterface $categoryService)
    {
       $this->categoryService = $categoryService;
    }

    // Tell don't ask! Guard for wrong mapping data
    public function guardForInvalidMaping($categoryName)
    {
        if (!isset($this->categoryNameToIdMapping[$categoryName]))
        {
            throw new \InvalidArgumentException(sprintf('Given category name [%s] is not mapped.', $categoryName));
        }
    }

    /**
     * @inheritdoc
     */
    public function getProductsForCategory($categoryName)
    {
        $this->guardForInvalidMaping($categoryName);

        $categoryId = $this->categoryNameToIdMapping[$categoryName];

        $productResults = $this->categoryService->getProducts($categoryId);

        // Do not forget algorithm for price ordering to reorder the products!!!!
        return $productResults;
    }
}