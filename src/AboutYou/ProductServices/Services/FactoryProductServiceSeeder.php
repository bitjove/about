<?php

namespace AboutYou\ProductServices\Services;

use AboutYou\CategoryServices\Services\CategoryService;
use AboutYou\ProductServices\Services\PriceOrderedProductService;
use AboutYou\ProductServices\Services\UnorderedProductService;

class FactoryProductServiceSeeder
{

    /**
     * This method is for seeding different Product services
     * and depend on passed string type.
     *
     * @param string $type
     *
     * @return ProductService
     */
    public static function for($type)
    {
        switch ($type) {
            case 'UnorderedProducts':
                $factory = new UnorderedProductService(new CategoryService);
                break;
            case 'PriceOrderedProducts':
                $factory = new PriceOrderedProductService(new CategoryService);
                break;
            default:
                throw new \InvalidArgumentException(sprintf('Given PriceService type name [%s] is not mapped.', $type));
        }
        return $factory;
    }
}
