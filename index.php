<?php

require "vendor/autoload.php";


use AboutYou\App;
use AboutYou\CategoryServices\Services\CategoryService;
use AboutYou\Data\Services\DataService;
use AboutYou\Data\Services\JsonDataLoggerService;
use AboutYou\EntitiesValidators\VariantValidator;
use AboutYou\ProductServices\Services\FactoryProductServiceSeeder;
use AboutYou\ProductServices\Services\UnorderedProductService;

/*
    |--------------------------------------------------------------------------
    | Logging Configuration Data
    |--------------------------------------------------------------------------
    |
    | We want to work with the json file that's why we pass JsonDataLoggerService.
    | If we want to work with some Database(Doctrine, Eloquent etc. ) we should pass
    | DatabaseDataLoggerService and so on..
    |
    | For example: Accept: application/x.SUBTYPE.v1+json
    |
    */
    $jsonDataInstance = new JsonDataLoggerService;
    // Use the json data in this case
    $jsonData = (new DataService($jsonDataInstance))->get();

    /** We are binding that into the container with a "helper class" App.php to make it clear and simple
     * without repeating everywhere in the code
     */
    App::bind('jsonData', $jsonData);



    /*
    |--------------------------------------------------------------------------
    | CategoryService Implementation
    |--------------------------------------------------------------------------
    |
    | Get products from CategoryService and pass the values from the json file into the corresponding
    | classes from the Entity namespace.
    |
    |
    */
    $categoryService = new CategoryService;
    $products = $categoryService->getProducts(17325);
    // echo '<pre>' . print_r($products , true) . '</pre>'; die();

    /*
    |--------------------------------------------------------------------------
    | Different implementations of the ProductServiceInterface
    |--------------------------------------------------------------------------
    |
    | You can get different Product Services through Seeder Factory class and get the result
    | by passing specific string for needed instance
    |
    */
    $unorderedProductsInstance = FactoryProductServiceSeeder::for('UnorderedProducts');
    $unorderedProducts = $unorderedProductsInstance->getProductsForCategory('Clothes');
    // echo '<pre>' . print_r($unorderedProducts, true) . '</pre>'; die();

    $priceOrderedProductsInstance = FactoryProductServiceSeeder::for('PriceOrderedProducts');
    $priceOrderedProducts = $priceOrderedProductsInstance->getProductsForCategory('Clothes');
    // echo '<pre>' . print_r($priceOrderedProducts , true) . '</pre>'; die();

    /*
    |--------------------------------------------------------------------------
    | Validation class is implemented in the CategoryService service
    |--------------------------------------------------------------------------
    |
    | Validation mechanism for Variant Entity's to ensure the values are valid.
    |
    */


die(var_dump('It is working boss! :)'));
